package de.antrophos.filebackup;

import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import javax.swing.UIManager;

/**
 * This is the main launcher of the application.
 *
 * @author Robert Breunung
 */
public class MainApp {

    private static final Logger logger = Logger.getLogger(MainApp.class.getCanonicalName());

    private static void loggerSetup() throws IOException {
        logger.setLevel(Level.ALL);
        FileHandler handler = new FileHandler("debug.log", 2097152, 1, true);
        SimpleFormatter formater = new SimpleFormatter();
        handler.setFormatter(formater);
        logger.addHandler(handler);
        logger.addHandler(new ConsoleHandler());
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) throws IOException {

        loggerSetup();

        try {
            javax.swing.UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainApp.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            System.exit(1);
        }

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MainGUI().setVisible(true);
            }
        });
    }
}
